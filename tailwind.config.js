module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false,
  important: true,
  theme: {
    screens: {
      s: '320px',
      sm: '480px',
      md: '768px',
      lg: '1200px',
      xl: '1460px',
      xxl: '1920px',
    },
    colors: {
      primary: '#EE3C29',
      accent: '#FEA402',
      black: '#333333',
      white: '#FFFFFF',
      gray: '#616161',
      'gray-light': '#777777',
      'bg-gray': '#F7F7F7',
      'c-border': '#D8D8D8',
      'bg-input': '#F0F2F7',
      'bg-facebook': '#3B5998',
      'bg-twitter': '#55ACEE',
      'bg-instagram': '#3A3A3A',
    },
    backgroundColor: (theme) => ({
      ...theme('colors'),
    }),
    fontFamily: {
      roboto: ['Roboto', 'sans-serif'],
      montserrat: ['Montserrat', 'sans-serif'],
    },
    extend: {},
  },
  corePlugins: {
    container: false,
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
