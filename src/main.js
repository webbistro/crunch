import { createApp } from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import VClickOutside from './directives/VClickOutside';
import Loader from './components/ui/AppLoader';

import '@/assets/styles/main.scss';

const app = createApp(App);

app.component('Loader', Loader);
app.directive('outside', VClickOutside);

app
  .use(store)
  .use(router)
  .mount('#app');
