export function dateFormat(value, format = 'date') {
  const options = {};

  if (format.includes('date')) {
    options.day = 'numeric';
    options.month = 'short';
    options.year = 'numeric';
  }
  return new Intl.DateTimeFormat('en-GB', options).format(new Date(value));
}
