const ERROR_CODES = {};

export function error(code) {
  return ERROR_CODES[code] ? ERROR_CODES[code] : 'Unknown error';
}
