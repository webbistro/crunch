import { createRouter, createWebHistory } from 'vue-router';
import store from '@/store';
import menRoutes from './men.routes';
import womenRoutes from './women.routes';
import kidsRoutes from './kids.routes';
import brandsRoutes from './brands.routes';
import releasesRoutes from './releases.routes';
import saleRoutes from './sale.routes';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/Home.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/auth/Login.vue'),
    meta: { layout: 'auth', auth: false },
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/auth/Register.vue'),
    meta: { layout: 'auth', auth: false },
  },
  {
    path: '/reset',
    name: 'Reset',
    component: () => import('../views/auth/Reset.vue'),
    meta: { layout: 'auth', auth: false },
  },
  {
    path: '/cart',
    name: 'Cart',
    component: () => import('../views/Cart.vue'),
    meta: { layout: 'main', auth: false },
  },
];

const routesAll = [...routes, ...menRoutes, ...womenRoutes, ...kidsRoutes,
  ...brandsRoutes, ...releasesRoutes, ...saleRoutes];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes: routesAll,
  linkActiveClass: 'active',
  linkExactActiveClass: 'active',
});

router.beforeEach((to, from, next) => {
  const requireAuth = to.meta.auth;
  const requireAdmin = to.meta.admin;

  if (requireAdmin) {
    if (store.getters['auth/isAdmin']) {
      return next();
    }
    return next('/login?message=admin');
  }

  if (requireAuth) {
    if (store.getters['auth/isAuthenticated']) {
      return next();
    }
    return next('/login?message=auth');
  }

  next();
});

export default router;
