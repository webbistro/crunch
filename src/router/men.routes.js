const menRoutes = [
  {
    path: '/men',
    name: 'Men',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/collections/mens-footwear',
    name: 'Mens Footwear',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/collections/mens-appare',
    name: 'Mens Appare',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/collections/mens-accessories',
    name: 'Mens Accessories',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
];

export default menRoutes;
