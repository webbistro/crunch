const saleRoutes = [
  {
    path: '/sale',
    name: 'Sale All',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/mens-sale',
    name: 'Mens Sale',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/womens-sale',
    name: 'Womens Sale',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/kids-sale',
    name: 'Kids Sale',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
];

export default saleRoutes;
