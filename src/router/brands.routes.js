const brandsRoutes = [
  {
    path: '/brands',
    name: 'Brands',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/collections/nike',
    name: 'Nike',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/collections/jordan',
    name: 'Jordan',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/collections/adidas',
    name: 'Adidas',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/collections/puma',
    name: 'Puma',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/collections/converse',
    name: 'Converse',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/all-brands',
    name: 'All Brands',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
];

export default brandsRoutes;
