const releasesRoutes = [
  {
    path: '/releases',
    name: 'Releases',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/new-men',
    name: 'New Men',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/new-women',
    name: 'New Women',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/new-kids',
    name: 'New Kids',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
];

export default releasesRoutes;
