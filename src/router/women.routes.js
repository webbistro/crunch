const womenRoutes = [
  {
    path: '/women',
    name: 'Women',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/collections/womens-footwear',
    name: 'Womens Footwear',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/collections/womens-appare',
    name: 'Womens Appare',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/collections/womens-accessories',
    name: 'Womens Accessories',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
];

export default womenRoutes;
