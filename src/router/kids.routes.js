const kidsRoutes = [
  {
    path: '/kids',
    name: 'Kids',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/collections/grade-school',
    name: 'Grade School',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/collections/pre-school',
    name: 'Pre School',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
  {
    path: '/collections/infant-toddler',
    name: 'Infant Toddler',
    component: () => import('../views/Product.vue'),
    meta: { layout: 'main', auth: false },
  },
];

export default kidsRoutes;
