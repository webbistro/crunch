const adminRoutes = [
  {
    path: '/admin',
    name: 'Admin',
    redirect: { name: 'AdminDashboard' },
    component: () => import('../views/admin/Admin.vue'),
    meta: {
      admin: true,
      layout: 'admin',
    },
    children: [
      {
        path: 'dashboard',
        name: 'AdminDashboard',
        component: () => import('../views/admin/Dashboard.vue'),
      },
      {
        path: 'products',
        name: 'AdminProducts',
        component: () => import('../views/admin/Products.vue'),
      },
      {
        path: 'product-add',
        name: 'AdminProductAdd',
        component: () => import('../views/admin/ProductAdd.vue'),
      },
      {
        path: 'categories',
        name: 'AdminCategories',
        component: () => import('../views/admin/Categories.vue'),
      },
      {
        path: 'brands',
        name: 'AdminBrands',
        component: () => import('../views/admin/Brands.vue'),
      },
      {
        path: 'orders',
        name: 'AdminOrders',
        component: () => import('../views/admin/Orders.vue'),
      },
      {
        path: 'product/:id',
        name: 'AdminProduct',
        component: () => import('../views/admin/Product.vue'),
      },
    ],
  },
];

export default adminRoutes;
