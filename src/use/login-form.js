import { ref, computed } from 'vue';
import useVuelidate from '@vuelidate/core';
import { required, email } from '@vuelidate/validators';

export function useLoginForm() {
  const email = ref(null);
  const password = ref(null);

  const rules = computed(() => ({
    email: { required, email },
    password: { required },
  }));

  return {
    email,
    password,
  };
}
