import { ref, computed } from 'vue';
import useVuelidate from '@vuelidate/core';
import { required, email } from '@vuelidate/validators';

export function useLoginForm() {
  const firstName = ref(null);
  const lastName = ref(null);
  const email = ref(null);
  const password = ref(null);

  return {
    firstName,
    lastName,
    email,
    password,
  };
}
